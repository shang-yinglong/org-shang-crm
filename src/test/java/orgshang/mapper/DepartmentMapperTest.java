package orgshang.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import orgshang.PermissionSystemAppStart;
import orgshang.org.mapper.DepartmentMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PermissionSystemAppStart.class)
public class DepartmentMapperTest {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Test
    public void add() {
    }

    @Test
    public void update() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void loadAll() {
    }

    @Test
    public void selectAll() {
        departmentMapper.selectAll().forEach(System.out::println);
    }
}