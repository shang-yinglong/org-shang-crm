package orgshang.base.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageList<T> {
    private Long total = 0L;
    private List<T> rows = new ArrayList<>();

}
