package orgshang.base.util;

//@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class AjaxResult {
    // 是否操作成功 true 成功 false失败
    private boolean success = true;
    // 提示信息
    private  String message = "操作成功";
    // 返回的数据 - 查询接口需要使用
    private Object data;

    public static AjaxResult me(){
        return new AjaxResult();
    }

    public boolean isSuccess() {
        return success;
    }

    public AjaxResult setSuccess(boolean success) {
        this.success = success;
        // 返回当前对象
        return this;
    }

    public String getMessage() {
        return message;
    }

    public AjaxResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public AjaxResult setData(Object data) {
        this.data = data;
        return this;
    }
}
