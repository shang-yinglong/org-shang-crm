package orgshang.base.util;

import org.springframework.util.StringUtils;
import orgshang.login.constant.LoginConstant;
import orgshang.org.domain.Employee;

/**
获取当前登录信息
 */
public class LoginContext {
    public static Employee getLoginUser(String token){
        if (StringUtils.isEmpty(token)){
            //如果token为空，我们正常要返回错误信息。目前是开发阶段为了方便如果token为空我们先返回一个默认对象@TODO
            Employee employee = new Employee();
            employee.setId(1L);
            employee.setUsername("admin");
            return employee;
        }else {
            return LoginConstant.loginMap.get(token);
        }

    }
}
