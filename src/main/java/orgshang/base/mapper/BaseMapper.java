package orgshang.base.mapper;

import lombok.Data;
import orgshang.base.query.BaseQuery;

import java.util.List;

public interface BaseMapper<T> {
    void add(T t);
    void update(T t);
    void delete(Long id);
    T loadAll(Long id);
    List<T> selectAll();

    long queryTotal(BaseQuery query);

    List<T> queryData(BaseQuery query);

    void batchDelete(List<Long> ids);
}
