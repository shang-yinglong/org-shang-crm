package orgshang.base.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BaseDomain {
    @ApiModelProperty(value = "唯一标识")
    private Long id;
}
