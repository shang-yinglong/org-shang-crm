package orgshang.base.service;

import orgshang.base.query.BaseQuery;
import orgshang.base.util.PageList;

import java.util.List;

public interface IBaseService<T> {
    void add(T t);
    void update(T t);
    void delete(Long id);
    T loadAll(Long id);
    List<T> selectAll();

    PageList<T> listPage(BaseQuery query);

    void batchDelete(List<Long> ids);
}
