package orgshang.base.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import orgshang.base.mapper.BaseMapper;
import orgshang.base.query.BaseQuery;
import orgshang.base.service.IBaseService;
import orgshang.base.util.PageList;

import java.util.List;
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements IBaseService<T> {
    @Autowired
    private BaseMapper<T> mapper;
    @Transactional
    @Override
    public void add(T t) {
        mapper.add(t);
    }
    @Transactional
    @Override
    public void update(T t) {
        mapper.update(t);
    }
    @Transactional
    @Override
    public void delete(Long id) {
        mapper.delete(id);
    }

    @Override
    public T loadAll(Long id) {
        return mapper.loadAll(id);
    }

    @Override
    public List<T> selectAll() {
        return mapper.selectAll();
    }

    @Override
    public PageList<T> listPage(BaseQuery query) {
        //查询总条数
        long total = mapper.queryTotal(query);
        if (total>0){
            List<T> rows = mapper.queryData(query);
            return new PageList<>(total,rows);
        }
        return new PageList<>();
    }

    @Override
    public void batchDelete(List<Long> ids) {
        mapper.batchDelete(ids);

    }
}
