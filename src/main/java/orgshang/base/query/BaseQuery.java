package orgshang.base.query;

import lombok.Data;

@Data
public class BaseQuery {
    private Integer currentPage = 1;//当前页
    private Integer pageSize = 10;//每页多少条
    private String keyword; // 关键字 如果name或intro含有这个关键字,就都展示
    // 在javabean里面,属性指的是Get set方法,上面定义的它只是字段而已  get()取值  set()赋值
    // 计算分页的起始条数
    // limit (当前页-1)*每页显示条数,每页显示条数
    public Integer getStart(){
        return (this.currentPage-1)*this.pageSize;
    }
}
