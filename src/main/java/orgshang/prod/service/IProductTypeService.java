package orgshang.prod.service;

import orgshang.prod.domain.ProductType;
import orgshang.base.service.IBaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
public interface IProductTypeService extends IBaseService<ProductType> {

    List<ProductType> tree();
}
