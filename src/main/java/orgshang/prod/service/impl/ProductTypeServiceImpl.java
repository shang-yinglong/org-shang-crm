package orgshang.prod.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import orgshang.prod.domain.ProductType;
import orgshang.prod.mapper.ProductTypeMapper;
import orgshang.prod.service.IProductTypeService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
@Service
public class ProductTypeServiceImpl extends BaseServiceImpl<ProductType> implements IProductTypeService {

    @Autowired
    private ProductTypeMapper productTypeMapper;
    @Override
    public List<ProductType> tree() {
        return productTypeMapper.tree();
    }
}
