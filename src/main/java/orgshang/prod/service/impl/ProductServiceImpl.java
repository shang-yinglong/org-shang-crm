package orgshang.prod.service.impl;

import orgshang.prod.domain.Product;
import orgshang.prod.service.IProductService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {

}
