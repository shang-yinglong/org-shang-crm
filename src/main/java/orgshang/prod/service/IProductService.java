package orgshang.prod.service;

import orgshang.prod.domain.Product;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
public interface IProductService extends IBaseService<Product> {

}
