package orgshang.prod.mapper;

import orgshang.prod.domain.ProductType;
import orgshang.base.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
public interface ProductTypeMapper extends BaseMapper<ProductType> {

    List<ProductType> tree();
}
