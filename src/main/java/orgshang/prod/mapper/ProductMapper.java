package orgshang.prod.mapper;

import orgshang.prod.domain.Product;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */
public interface ProductMapper extends BaseMapper<Product> {

}
