package orgshang.prod.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */

@Data
public class Product extends BaseDomain{

    private static final long serialVersionUID = 1L;


    private String name;

    private BigDecimal price;

    private String description;

/**
* 图片、视频地址
*/
    private String resource;

    private Long typeId;

    private ProductType type;

}
