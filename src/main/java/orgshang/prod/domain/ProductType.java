package orgshang.prod.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-25
 */

@Data
public class ProductType extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
    private String name;

    private String description;

    private Long parentId;

    private ProductType parent;
    //子类型，二级部门
    private List<ProductType> children;
    
}
