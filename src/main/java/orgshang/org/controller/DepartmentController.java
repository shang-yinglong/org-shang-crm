package orgshang.org.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import orgshang.anno.RonghuaPermission;
import orgshang.org.domain.Department;
import orgshang.org.query.DepartmentQuery;
import orgshang.org.service.IDepartmentService;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/department")
@Api(value = "部门接口",description = "部门接口的增删改查")
@RonghuaPermission(name="部门管理")
public class DepartmentController {
    @Autowired
    private IDepartmentService departmentService;

    @PatchMapping
    @ApiOperation("批量删除")
    public AjaxResult batchDelete(@RequestBody List<Long> ids) {
        try {
            departmentService.batchDelete(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!"+e.getMessage());
        }

    }
    @RonghuaPermission(name = "根据id来删除")
    @ApiOperation("删除")
    @DeleteMapping("/{id}")
    public AjaxResult deleteById(@PathVariable("id") Long id) {
        try {
            departmentService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败!");
        }

    }
    @RonghuaPermission(name ="根据id查询")
    @GetMapping("/{id}")
    public AjaxResult getById(@PathVariable("id") Long id) {
        try {
            Department department = departmentService.loadAll(id);
            return AjaxResult.me().setData(department);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败" + e.getMessage());
        }

    }

    @GetMapping
    public AjaxResult getAll() {
        try {
            List<Department> departments = departmentService.selectAll();
            return AjaxResult.me().setData(departments);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败" + e.getMessage());
        }

    }

    @PutMapping
    @ApiOperation("添加/修改部门")
    public AjaxResult addOrUpdate(@RequestBody Department department) {
        System.out.println(department);
        try {
            if (Objects.isNull(department.getId())) {
                departmentService.add(department);
            } else {
                departmentService.update(department);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败" + e.getMessage());
        }
    }
    @PostMapping
    public AjaxResult listPage(@RequestBody DepartmentQuery query){
        try {
           PageList<Department> result =  departmentService.listPage(query);
           return AjaxResult.me().setData(result);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("分页查询失败"+e.getMessage());
        }

    }
    @GetMapping("/tree")
    public AjaxResult tree(){
        try {
           List<Department> departments =  departmentService.tree();
           return AjaxResult.me().setData(departments);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败");
        }
    }

}
