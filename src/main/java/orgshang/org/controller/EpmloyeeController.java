package orgshang.org.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import orgshang.org.domain.Employee;
import orgshang.org.query.EmployeeQuery;
import orgshang.org.service.impl.EmployeeServiceImpl;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/employee")
public class EpmloyeeController {
    @Autowired
    private EmployeeServiceImpl employeeService;
    @GetMapping
    public AjaxResult loadAll(){
        try {
           List<Employee> employees  = employeeService.selectAll();
           return AjaxResult.me().setData(employees);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("员工查询失败");
        }
    }
    @GetMapping("/{id}")
    public AjaxResult loadByID(@PathVariable("id") Long id){
        try {
            Employee employee = employeeService.loadAll(id);
            return AjaxResult.me().setData(employee);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败"+e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            employeeService.delete(id);
           return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败"+e.getMessage());
        }

    }
    @PutMapping
    public AjaxResult addAndsave(@RequestBody Employee employee){
        try {
            if (Objects.isNull(employee.getId())){
                employeeService.add(employee);
            }else {
                employeeService.update(employee);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("操作失败"+e.getMessage());
        }

    }
    @PostMapping
    public AjaxResult page(@RequestBody EmployeeQuery query){
        try {
            PageList<Employee> page = employeeService.listPage(query);
            return AjaxResult.me().setData(page);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("分页查询失败"+e.getMessage());
        }

    }
    @PatchMapping
    public AjaxResult batchdelete(@RequestBody List<Long>ids){
        try {
            employeeService.batchDelete(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("批量删除失败"+e.getMessage());
        }

    }
}
