package orgshang.org.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import orgshang.base.domain.BaseDomain;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "部门域对象")
public class Department extends BaseDomain {
   private String name;
   private String intro;
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
   private Date create_time;
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
   private Date update_time;
   private Long manager_id;
   private Long parent_id;
   private String path;
   private Integer state;
   //部门经理多对一关系
   private Employee manager;
   //父部门多对一关系
   private Department parent;
   //一个父部门会有多个子部门
   private List<Department> children;
}
