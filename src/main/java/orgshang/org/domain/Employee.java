package orgshang.org.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import orgshang.base.domain.BaseDomain;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee extends BaseDomain {
    private String username;

    private String password;
    private String email;
    private String headImage;
    private Integer age;

    private Department department;


}
