package orgshang.org.service;

import orgshang.base.service.IBaseService;
import orgshang.org.domain.Employee;
import orgshang.org.query.EmployeeQuery;
import orgshang.base.util.PageList;

import java.util.List;
public interface IEmployeeService extends IBaseService<Employee> {

    Employee getByUsername(String username);
}
