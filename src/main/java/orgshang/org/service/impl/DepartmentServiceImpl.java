package orgshang.org.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import orgshang.base.service.impl.BaseServiceImpl;
import orgshang.org.mapper.DepartmentMapper;
import orgshang.org.domain.Department;
import orgshang.org.service.IDepartmentService;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements IDepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;
    @Transactional
    @Override
    public void add(Department department) {
        //设置新增时间
        department.setCreate_time(new Date());
        departmentMapper.add(department);
        //如果是以及部门path=/家自己的id
        //如果不是一级部门path=父部门的id+自己的id
        Department parent = department.getParent();
        String path = "";
        if (Objects.isNull(parent) ||Objects.isNull(parent.getId())){//他为空说明前端没有选择父部门，那他当前就是一级部门
            path = "/"+department.getId();
        }else {
            //拿到当前部门父部门的信息
           parent = departmentMapper.loadAll(parent.getId());
          path = parent.getPath()+"/"+department.getId();
        }
        System.out.println(path);
        department.setPath(path);
        departmentMapper.update(department);


    }
    @Transactional
    @Override
    public void update(Department department) {
        //修改时间
        department.setUpdate_time(new Date());
        Department parent = department.getParent();
        String path = "";
        if (Objects.isNull(parent) ||Objects.isNull(parent.getId())){
            path = "/"+department.getId();
        }else {
            //拿到当前部门父部门的信息
            parent = departmentMapper.loadAll(parent.getId());
            path = parent.getPath()+"/"+department.getId();
        }
        department.setPath(path);
        System.out.println(path);
        departmentMapper.update(department);

    }
    @Override
    public List<Department> tree() {
        return departmentMapper.tree();
    }


}
