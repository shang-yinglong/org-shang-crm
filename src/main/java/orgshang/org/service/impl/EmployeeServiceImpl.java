package orgshang.org.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import orgshang.base.service.impl.BaseServiceImpl;
import orgshang.org.mapper.EmployeeMapper;
import orgshang.org.domain.Employee;
import orgshang.org.query.EmployeeQuery;
import orgshang.org.service.IEmployeeService;
import orgshang.base.util.PageList;

import java.util.List;
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;


    @Override
    public Employee getByUsername(String username) {
        return employeeMapper.geByUsername(username);
    }


}
