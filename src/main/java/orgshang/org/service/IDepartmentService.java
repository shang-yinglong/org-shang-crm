package orgshang.org.service;

import orgshang.base.service.IBaseService;
import orgshang.org.domain.Department;

import java.util.List;

public interface IDepartmentService extends IBaseService<Department> {

    List<Department> tree();
}
