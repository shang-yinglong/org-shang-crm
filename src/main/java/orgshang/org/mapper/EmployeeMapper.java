package orgshang.org.mapper;

import orgshang.base.mapper.BaseMapper;
import orgshang.org.domain.Employee;
import orgshang.org.query.EmployeeQuery;

import java.util.List;

public interface EmployeeMapper extends BaseMapper<Employee> {

    Employee geByUsername(String username);
}
