package orgshang.org.mapper;

import orgshang.base.mapper.BaseMapper;
import orgshang.org.domain.Department;
import orgshang.org.query.DepartmentQuery;

import java.util.List;

public interface DepartmentMapper extends BaseMapper<Department> {
    //独有方法
    List<Department> tree();
}
