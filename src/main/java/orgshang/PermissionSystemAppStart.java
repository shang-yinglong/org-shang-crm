package orgshang;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import orgshang.interceptor.LoginInterceptor;

/**
 * 1111
 */
@SpringBootApplication
@MapperScan("orgshang.*.mapper")
@ServletComponentScan("orgshang.listener")
public class PermissionSystemAppStart implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(PermissionSystemAppStart.class,args);
    }
    @Autowired
    private LoginInterceptor loginInterceptor;
    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/login","/logout");
    }
}
