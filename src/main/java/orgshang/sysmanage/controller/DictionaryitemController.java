package orgshang.sysmanage.controller;

import orgshang.sysmanage.service.IDictionaryitemService;
import orgshang.sysmanage.domain.Dictionaryitem;
import orgshang.sysmanage.query.DictionaryitemQuery;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dictionaryitem")
public class DictionaryitemController {
    @Autowired
    public IDictionaryitemService dictionaryitemService;


    /**
     * 保存和修改公用的
     * @param dictionaryitem  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Dictionaryitem dictionaryitem){
        try {
            if( dictionaryitem.getId()!=null)
                dictionaryitemService.update(dictionaryitem);
            else
                dictionaryitemService.add(dictionaryitem);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            dictionaryitemService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            Dictionaryitem dictionaryitem = dictionaryitemService.loadAll(id);
            return AjaxResult.me().setData(dictionaryitem);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }
    @GetMapping("/get/{sn}")
    public AjaxResult getBySn(@PathVariable("sn")String sn) {
        try {
            List<Dictionaryitem> list = dictionaryitemService.getBySn(sn);
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public AjaxResult list(){

        try {
            List< Dictionaryitem> list = dictionaryitemService.selectAll();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public AjaxResult json(@RequestBody DictionaryitemQuery query)
    {
        try {
            PageList<Dictionaryitem> pageList = dictionaryitemService.listPage(query);
            return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }
}
