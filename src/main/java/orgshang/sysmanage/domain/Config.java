package orgshang.sysmanage.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */

@Data
public class Config extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
        /**
         * 配置信息的key
         */
                                    private String key;
    
        /**
         * 配置信息的值
         */
                                    private String value;
    
                                    private Long type;
    
        /**
         * 简介
         */
                                    private String intro;
    
}
