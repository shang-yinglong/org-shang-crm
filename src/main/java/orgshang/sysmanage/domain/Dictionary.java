package orgshang.sysmanage.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */

@Data
public class Dictionary extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
        /**
         * 标识
         */
                                    private String sn;
    
        /**
         * 标题
         */
                                    private String title;
    
        /**
         * 描述
         */
                                    private String intro;
    
                                    private Integer status;
    
}
