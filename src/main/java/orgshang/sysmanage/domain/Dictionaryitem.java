package orgshang.sysmanage.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */

@Data
public class Dictionaryitem extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String title;
    
                                    private String value;
    
                                    private Integer sequence;
    
                                    private String intro;
    
        /**
         * 数据字典类型id
         */
                                    private Long parentId;
    
}
