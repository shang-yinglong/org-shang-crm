package orgshang.sysmanage.mapper;

import orgshang.sysmanage.domain.Dictionaryitem;
import orgshang.base.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface DictionaryitemMapper extends BaseMapper<Dictionaryitem> {

    List<Dictionaryitem> getBySn(String sn);
}
