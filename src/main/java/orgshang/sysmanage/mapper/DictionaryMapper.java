package orgshang.sysmanage.mapper;

import orgshang.sysmanage.domain.Dictionary;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}
