package orgshang.sysmanage.service;

import orgshang.sysmanage.domain.Dictionary;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface IDictionaryService extends IBaseService<Dictionary> {

}
