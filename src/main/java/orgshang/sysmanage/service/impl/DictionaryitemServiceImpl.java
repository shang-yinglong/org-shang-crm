package orgshang.sysmanage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import orgshang.sysmanage.domain.Dictionaryitem;
import orgshang.sysmanage.mapper.DictionaryitemMapper;
import orgshang.sysmanage.service.IDictionaryitemService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
@Service
public class DictionaryitemServiceImpl extends BaseServiceImpl<Dictionaryitem> implements IDictionaryitemService {

    @Autowired
    private DictionaryitemMapper dictionaryitemMapper;
    @Override
    public List<Dictionaryitem> getBySn(String sn) {
        return dictionaryitemMapper.getBySn(sn);
    }
}
