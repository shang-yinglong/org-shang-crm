package orgshang.sysmanage.service.impl;

import orgshang.sysmanage.domain.Config;
import orgshang.sysmanage.service.IConfigService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
@Service
public class ConfigServiceImpl extends BaseServiceImpl<Config> implements IConfigService {

}
