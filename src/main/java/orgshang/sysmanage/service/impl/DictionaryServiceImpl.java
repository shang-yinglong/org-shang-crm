package orgshang.sysmanage.service.impl;

import orgshang.sysmanage.domain.Dictionary;
import orgshang.sysmanage.service.IDictionaryService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
@Service
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary> implements IDictionaryService {

}
