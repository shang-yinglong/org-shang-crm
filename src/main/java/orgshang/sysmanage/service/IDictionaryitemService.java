package orgshang.sysmanage.service;

import orgshang.sysmanage.domain.Dictionaryitem;
import orgshang.base.service.IBaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface IDictionaryitemService extends IBaseService<Dictionaryitem> {

    List<Dictionaryitem> getBySn(String sn);
}
