package orgshang.mkt.service;

import orgshang.mkt.domain.ClueRemark;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface IClueRemarkService extends IBaseService<ClueRemark> {

}
