package orgshang.mkt.service;

import orgshang.mkt.domain.BusinessRemark;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface IBusinessRemarkService extends IBaseService<BusinessRemark> {

}
