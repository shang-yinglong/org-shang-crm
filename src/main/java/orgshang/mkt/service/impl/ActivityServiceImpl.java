package orgshang.mkt.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import orgshang.base.util.LoginContext;
import orgshang.mkt.domain.Activity;
import orgshang.mkt.domain.ActivityRemark;
import orgshang.mkt.mapper.ActivityMapper;
import orgshang.mkt.mapper.ActivityRemarkMapper;
import orgshang.mkt.service.IActivityService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import orgshang.org.domain.Employee;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
@Service
public class ActivityServiceImpl extends BaseServiceImpl<Activity> implements IActivityService {

    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityRemarkMapper activityRemarkMapper;
    @Transactional
    @Override
    public void add(Activity activity) {
        Employee loginUser = LoginContext.getLoginUser("");
        activity.setCreateBy(loginUser.getUsername());
        activity.setCreateTime(new Date());
        //创建时间 创建人
        super.add(activity);
        //新增活动备注
        ActivityRemark activityRemark = new ActivityRemark();
        activityRemark.setNoteContent("新增内容"+activity.getName());
        activityRemark.setCreateBy(loginUser.getUsername());
        activityRemark.setCreateTime(new Date());
        activityRemark.setActivityId(activity.getId());
        activityRemarkMapper.add(activityRemark);

    }
    @Transactional
    @Override
    public void update(Activity activity) {
        Employee loginUser = LoginContext.getLoginUser("");
        activity.setEditBy(loginUser.getUsername());
        activity.setEditTime(new Date());
        //修改时间 修改人
        //修改活动备注
        ActivityRemark activityRemark = new ActivityRemark();
        activityRemark.setNoteContent("新增内容"+activity.getName());
        activityRemark.setCreateBy(loginUser.getUsername());
        activityRemark.setCreateTime(new Date());
        activityRemark.setActivityId(activity.getId());
        activityRemarkMapper.add(activityRemark);
        super.update(activity);
    }

    @Override
    public List<Activity> getByType(Integer type) {
        return activityMapper.getByType(type);
    }
}
