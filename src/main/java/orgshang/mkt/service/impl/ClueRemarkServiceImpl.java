package orgshang.mkt.service.impl;

import orgshang.mkt.domain.ClueRemark;
import orgshang.mkt.service.IClueRemarkService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
@Service
public class ClueRemarkServiceImpl extends BaseServiceImpl<ClueRemark> implements IClueRemarkService {

}
