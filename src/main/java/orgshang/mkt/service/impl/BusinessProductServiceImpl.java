package orgshang.mkt.service.impl;

import orgshang.mkt.domain.BusinessProduct;
import orgshang.mkt.service.IBusinessProductService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
@Service
public class BusinessProductServiceImpl extends BaseServiceImpl<BusinessProduct> implements IBusinessProductService {

}
