package orgshang.mkt.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import orgshang.mkt.domain.ClueActivity;
import orgshang.mkt.dto.ClueActivityDto;
import orgshang.mkt.mapper.ClueActivityMapper;
import orgshang.mkt.service.IClueActivityService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
@Service
public class ClueActivityServiceImpl extends BaseServiceImpl<ClueActivity> implements IClueActivityService {

    @Autowired
    private ClueActivityMapper clueActivityMapper;
    @Override
    public List<Long> getBindActivityIds(Long clueId) {
        return clueActivityMapper.getBindActivityIds(clueId);
    }

    @Override
    public void bindActivity(ClueActivityDto dto) {
        //删除这个线索所关联的活动，为了解决重复绑定
        clueActivityMapper.deleteByClue(dto.getClueId());
        //有几个活动，关联表就要有几个数据
        //批量添加
        clueActivityMapper.batchAdd(dto);
    }
}
