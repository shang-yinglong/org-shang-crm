package orgshang.mkt.service;

import orgshang.mkt.domain.ClueActivity;
import orgshang.base.service.IBaseService;
import orgshang.mkt.dto.ClueActivityDto;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface IClueActivityService extends IBaseService<ClueActivity> {

    List<Long> getBindActivityIds(Long clueId);

    void bindActivity(ClueActivityDto dto);
}
