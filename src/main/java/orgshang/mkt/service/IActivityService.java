package orgshang.mkt.service;

import orgshang.mkt.domain.Activity;
import orgshang.base.service.IBaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface IActivityService extends IBaseService<Activity> {

    List<Activity> getByType(Integer type);
}
