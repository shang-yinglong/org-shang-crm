package orgshang.mkt.controller;

import orgshang.mkt.dto.ClueActivityDto;
import orgshang.mkt.service.IClueActivityService;
import orgshang.mkt.domain.ClueActivity;
import orgshang.mkt.query.ClueActivityQuery;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clueActivity")
public class ClueActivityController {
    @Autowired
    public IClueActivityService clueActivityService;


    /**
     * 保存和修改公用的
     * @param clueActivity  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ClueActivity clueActivity){
        try {
            if( clueActivity.getId()!=null)
                clueActivityService.update(clueActivity);
            else
                clueActivityService.add(clueActivity);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            clueActivityService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            ClueActivity clueActivity = clueActivityService.loadAll(id);
            return AjaxResult.me().setData(clueActivity);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public AjaxResult list(){

        try {
            List< ClueActivity> list = clueActivityService.selectAll();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }

    /**
     * 通过线索id绑定的活动id
     * @param clueId
     * @return
     */
    @GetMapping("/bind/{clueId}")
    public AjaxResult getBindActivityIds(@PathVariable("clueId")Long clueId){
        try {
            List<Long> list = clueActivityService.getBindActivityIds(clueId);
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }
    @PostMapping("/bind")
    public AjaxResult bindActivity(@RequestBody ClueActivityDto dto){
        try {
            clueActivityService.bindActivity(dto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存线索失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public AjaxResult json(@RequestBody ClueActivityQuery query)
    {
        try {
            PageList<ClueActivity> pageList = clueActivityService.listPage(query);
            return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }
}
