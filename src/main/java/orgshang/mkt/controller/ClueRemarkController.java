package orgshang.mkt.controller;

import orgshang.mkt.service.IClueRemarkService;
import orgshang.mkt.domain.ClueRemark;
import orgshang.mkt.query.ClueRemarkQuery;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clueRemark")
public class ClueRemarkController {
    @Autowired
    public IClueRemarkService clueRemarkService;


    /**
     * 保存和修改公用的
     * @param clueRemark  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ClueRemark clueRemark){
        try {
            if( clueRemark.getId()!=null)
                clueRemarkService.update(clueRemark);
            else
                clueRemarkService.add(clueRemark);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            clueRemarkService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            ClueRemark clueRemark = clueRemarkService.loadAll(id);
            return AjaxResult.me().setData(clueRemark);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public AjaxResult list(){

        try {
            List< ClueRemark> list = clueRemarkService.selectAll();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public AjaxResult json(@RequestBody ClueRemarkQuery query)
    {
        try {
            PageList<ClueRemark> pageList = clueRemarkService.listPage(query);
            return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }
}
