package orgshang.mkt.controller;

import orgshang.mkt.service.IBusinessRemarkService;
import orgshang.mkt.domain.BusinessRemark;
import orgshang.mkt.query.BusinessRemarkQuery;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/businessRemark")
public class BusinessRemarkController {
    @Autowired
    public IBusinessRemarkService businessRemarkService;


    /**
     * 保存和修改公用的
     * @param businessRemark  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody BusinessRemark businessRemark){
        try {
            if( businessRemark.getId()!=null)
                businessRemarkService.update(businessRemark);
            else
                businessRemarkService.add(businessRemark);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            businessRemarkService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id")Long id)
    {
        try {
            BusinessRemark businessRemark = businessRemarkService.loadAll(id);
            return AjaxResult.me().setData(businessRemark);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取一个失败！"+e.getMessage());
        }
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public AjaxResult list(){

        try {
            List< BusinessRemark> list = businessRemarkService.selectAll();
            return AjaxResult.me().setData(list);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取所有失败！"+e.getMessage());
        }
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public AjaxResult json(@RequestBody BusinessRemarkQuery query)
    {
        try {
            PageList<BusinessRemark> pageList = businessRemarkService.listPage(query);
            return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取分页数据失败！"+e.getMessage());
        }
    }
}
