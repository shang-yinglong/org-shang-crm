package orgshang.mkt.dto;

import lombok.Data;

import java.util.List;

@Data
public class ClueActivityDto {
    private Long clueId;
    private List<Long> activityIds;
}
