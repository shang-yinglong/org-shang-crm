package orgshang.mkt.mapper;

import orgshang.mkt.domain.Business;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface BusinessMapper extends BaseMapper<Business> {

}
