package orgshang.mkt.mapper;

import orgshang.mkt.domain.ClueActivity;
import orgshang.base.mapper.BaseMapper;
import orgshang.mkt.dto.ClueActivityDto;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface ClueActivityMapper extends BaseMapper<ClueActivity> {

    List<Long> getBindActivityIds(Long clueId);

    void batchAdd(ClueActivityDto dto);

    void deleteByClue(Long clueId);
}
