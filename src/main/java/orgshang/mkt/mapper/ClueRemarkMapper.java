package orgshang.mkt.mapper;

import orgshang.mkt.domain.ClueRemark;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface ClueRemarkMapper extends BaseMapper<ClueRemark> {

}
