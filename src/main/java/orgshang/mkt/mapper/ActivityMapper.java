package orgshang.mkt.mapper;

import orgshang.mkt.domain.Activity;
import orgshang.base.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */
public interface ActivityMapper extends BaseMapper<Activity> {

    List<Activity> getByType(Integer type);
}
