package orgshang.mkt.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;
import orgshang.org.domain.Employee;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */

@Data
public class Clue extends BaseDomain{

    private static final long serialVersionUID = 1L;


/**
* 全名
*/
    private String fullName;

/**
* 称呼
*/
    private String appellation;

    private String company;

    private String job;

    private String email;

    private String phone;

    private String mphone;

    private String address;

/**
* 客户来源，关联向数据字典明细
*/
    private Long source;

/**
* 营销人员，关联向员工表
*/
    private Long owner;
    private Employee seller;

/**
* 1-未分配  2-跟进中 3-已转化为商机 0-放入线索池
*/
    private Integer state;

    private String createBy;

    private Date createTime;

    private String editBy;

    private Date editTime;

    private String description;

/**
* 最近一次跟进概述
*/
    private String contactSummary;

/**
* 下一次联系的时间
*/
    private Date nextContactTime;

}
