package orgshang.mkt.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */

@Data
public class Activity extends BaseDomain{

    private static final long serialVersionUID = 1L;


    private String name;

/**
* 1-市场活动 2-营销活动
*/
    private Integer type;

/**
* 活动的优惠方式
*/
    private Long preferentialMethodId;
    @JsonFormat(pattern = "yyy-MM-dd",timezone = "GMT+8")
    private Date startDate;
    @JsonFormat(pattern = "yyy-MM-dd",timezone = "GMT+8")
    private Date endDate;

    private String description;
    @JsonFormat(pattern = "yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

/**
* 谁创建的活动
*/
    private String createBy;
    @JsonFormat(pattern = "yyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date editTime;

/**
* 谁最后一次修改
*/
    private String editBy;

}
