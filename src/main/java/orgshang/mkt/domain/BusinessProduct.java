package orgshang.mkt.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */

@Data
public class BusinessProduct extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String name;
    
                                    private BigDecimal price;
    
                                    private String description;
    
                                    private Long typeId;
    
                                    private String resource;
    
}
