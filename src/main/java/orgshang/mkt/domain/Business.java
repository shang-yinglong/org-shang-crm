package orgshang.mkt.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */

@Data
public class Business extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String name;
    
                                    private Long clueId;
    
        /**
         * 关联商品表
         */
                                    private Long productId;
    
        /**
         * 关联商机商品表
         */
                                    private Long businessProductId;
    
        /**
         * 产生商机那一刻的商品名字
         */
                                    private String productName;
    
        /**
         * 产生商机那一刻的商品价格
         */
                                    private BigDecimal productPrice;
    
        /**
         * 0 跟进中 1 缴纳定金 2 成单 -1 商机池
         */
                                    private Integer state;
    
}
