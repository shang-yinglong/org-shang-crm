package orgshang.mkt.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-26
 */

@Data
public class ClueRemark extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String noteContent;
    
                                    private String createBy;
    
                                    private Date createTime;
    
                                    private String editBy;
    
                                    private Date editTime;
    
                                    private String editFlag;
    
                                    private Long clueId;
    
}
