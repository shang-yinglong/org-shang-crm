package orgshang.listener;

import org.springframework.beans.factory.annotation.Autowired;
import orgshang.permission.service.IPermissionScanService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class PermissionScanListener implements ServletContextListener {
    @Autowired
    private IPermissionScanService permissionScanService;
    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        System.out.println("容器初始化开始");
//        permissionScanService.scan();
        System.out.println("容器初始化结束");
    }


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("初始化结束");

    }
}
