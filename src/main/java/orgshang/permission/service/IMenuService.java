package orgshang.permission.service;

import org.springframework.stereotype.Service;
import orgshang.permission.domain.Menu;

import java.util.List;

public interface IMenuService {
    List<Menu> tree(Long userId);
}
