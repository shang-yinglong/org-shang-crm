package orgshang.permission.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import orgshang.permission.query.PermissionQuery;
import orgshang.permission.domain.Permission;
import orgshang.permission.mapper.PermissionMapper;
import orgshang.permission.service.IPermissionService;
import orgshang.base.util.PageList;

import java.util.List;
@Service
public class PermissionServiceImpl implements IPermissionService {

    @Autowired
    private PermissionMapper permissionMapper;
    @Override
    public PageList<Permission> listPage(PermissionQuery query) {
        //拿到总条数
        long total = permissionMapper.loadTotal(query);
        // 2 如果总条数>0,就根据query查询当前页数据
        if (total>0){
           List<Permission> rows =  permissionMapper.loadData(query);
           return new PageList<>(total,rows);
        }
        return new PageList<>();
    }

    @Override
    public List<String> getPermissionSnsByLoginUserId(Long id) {
        return permissionMapper.getPermissionSnsByLoginUserId();
    }
}
