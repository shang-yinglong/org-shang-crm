package orgshang.permission.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import orgshang.permission.domain.Menu;
import orgshang.permission.mapper.MenuMapper;
import orgshang.permission.service.IMenuService;

import java.util.List;
@Service
public class MenuServiceImpl implements IMenuService {
    @Autowired
    private MenuMapper menuMapper;
    @Override
    public List<Menu> tree(Long userId) {
        return menuMapper.tree(userId);
    }
}
