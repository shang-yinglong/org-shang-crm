package orgshang.permission.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import orgshang.permission.domain.Role;
import orgshang.permission.dto.RolePermissionDto;
import orgshang.permission.mapper.RoleMapper;
import orgshang.permission.query.RoleQuery;
import orgshang.permission.service.IRoleService;
import orgshang.base.util.PageList;

import java.util.List;
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper rolemapper;
    @Override
    public void add(Role role) {
                    rolemapper.insert(role);

    }
    @Transactional
    @Override
    public void update(Role role) {
        rolemapper.updata(role);
    }

    @Override
    public Role getById(Long id) {
        return rolemapper.getById(id);
    }

    @Override
    public List<Role> getAll() {
        return rolemapper.getAll();
    }

    @Override
    public PageList<Role> listPage(RoleQuery query) {
        //查询总条数
        long total = rolemapper.queryTotal(query);
        if(total>0){
            List<Role> rows = rolemapper.queryData(query);
            return new PageList<>(total,rows);
        }
        return new PageList<>();
    }
    @Transactional
    @Override
    public void deleteById(Long id) {
        rolemapper.deleteById(id);
    }
    @Transactional
    @Override
    public void batchDelete(List<Long> ids) {
        rolemapper.batchDelete(ids);
    }

    @Override
    public void saveRolePermission(RolePermissionDto dto) {
        // 当前角色有可能之前已经存在一些权限了,怎么办?
        // 1 先删除这个角色原来所有的权限
        rolemapper.deletePermissionByRoleId(dto.getRoleId());

        // 2 保存这个角色的权限
        // 一个角色会对应多个权限,每一个权限,都是一个关联关系
        // 方式一:循环List<String> permissionSns 每次循环都调用mapper做新增
        // 方式二:批量新增
        //roleMapper.batchSaveRolePermission(dto);
        rolemapper.batchSaveRolePermission(dto.getRoleId(),dto.getPermissionSns());
    }

    @Override
    public List<String> getRolePermissionSns(Long roleId) {
        return rolemapper.getRolePermissionSns(roleId);
    }

}
