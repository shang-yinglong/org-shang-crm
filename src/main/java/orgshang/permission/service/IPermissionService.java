package orgshang.permission.service;

import orgshang.permission.query.PermissionQuery;
import orgshang.permission.domain.Permission;
import orgshang.base.util.PageList;

import java.util.List;

public interface IPermissionService {

    PageList<Permission> listPage(PermissionQuery query);

    List<String> getPermissionSnsByLoginUserId(Long id);
}
