package orgshang.permission.service;

import orgshang.permission.domain.Role;
import orgshang.permission.dto.RolePermissionDto;
import orgshang.permission.query.RoleQuery;
import orgshang.base.util.PageList;

import java.util.List;

public interface IRoleService {

    void add(Role role);

    void update(Role role);

    Role getById(Long id);

    List<Role> getAll();

    PageList<Role> listPage(RoleQuery query);

    void deleteById(Long id);

    void batchDelete(List<Long> ids);

    void saveRolePermission(RolePermissionDto dto);

    List<String> getRolePermissionSns(Long roleId);
}
