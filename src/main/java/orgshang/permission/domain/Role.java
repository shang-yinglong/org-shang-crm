package orgshang.permission.domain;

import lombok.Data;

@Data
public class Role {
    private Long id;
    private String name;
    private String sn;
}
