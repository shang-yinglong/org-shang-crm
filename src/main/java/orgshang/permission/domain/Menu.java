package orgshang.permission.domain;

import lombok.Data;

import java.util.List;
@Data
public class Menu {
    private Long id;
    private String name;
    private String url;
    private String icon;
    // 父菜单
    private Menu parent;
    // 子菜单 - 多个
    private List<Menu> children;
}
