package orgshang.permission.domain;

import lombok.Data;
import orgshang.base.domain.BaseDomain;

@Data
public class Permission extends BaseDomain {
   private String name;
   private String url;
   private String descs;
   private String sn;
   //多对一关系
    Permission parent;

}
