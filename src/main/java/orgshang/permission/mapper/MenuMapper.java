package orgshang.permission.mapper;

import orgshang.permission.domain.Menu;

import java.util.List;

public interface MenuMapper {
    List<Menu> tree(Long userId);
}
