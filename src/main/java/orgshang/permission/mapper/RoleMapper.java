package orgshang.permission.mapper;

import org.apache.ibatis.annotations.Param;
import orgshang.permission.domain.Role;
import orgshang.permission.query.RoleQuery;

import java.util.List;

public interface RoleMapper {

    void insert(Role role);

    void updata(Role role);

    Role getById(Long id);

    List<Role> getAll();

    long queryTotal(RoleQuery query);

    List<Role> queryData(RoleQuery query);

    void deleteById(Long id);

    void batchDelete(List<Long> ids);

    void deletePermissionByRoleId(Long roleId);

    void batchSaveRolePermission(@Param("roleId") Long roleId, @Param("permissionSns") List<String> permissionSns);

    List<String> getRolePermissionSns(Long roleId);
}
