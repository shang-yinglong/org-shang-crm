package orgshang.permission.mapper;

import orgshang.permission.query.PermissionQuery;
import orgshang.permission.domain.Permission;

import java.util.List;

public interface PermissionMapper {
    long loadTotal(PermissionQuery query);

    List<Permission> loadData(PermissionQuery query);

    void insert(Permission permission);

    void deleteAll();

    List<String> getPermissionSnsByLoginUserId();
}
