package orgshang.permission.dto;

import lombok.Data;

import java.util.List;
@Data
public class RolePermissionDto {
    private Long roleId;
    private List<String> permissionSns;
}
