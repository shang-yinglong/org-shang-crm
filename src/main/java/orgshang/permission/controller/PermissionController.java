package orgshang.permission.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import orgshang.anno.RonghuaPermission;
import orgshang.permission.query.PermissionQuery;
import orgshang.permission.domain.Permission;
import orgshang.permission.service.IPermissionService;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;

@RonghuaPermission(name="权限管理")
@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private IPermissionService permissionService;

    @RonghuaPermission(name = "分页查询")
    @PostMapping
    public AjaxResult listPage(@RequestBody PermissionQuery query){
        try {
           PageList<Permission> pageList =  permissionService.listPage(query);
           return AjaxResult.me().setData(pageList);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("分页失败"+e.getMessage());
        }

    }

}
