package orgshang.permission.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import orgshang.permission.domain.Menu;
import orgshang.permission.service.impl.MenuServiceImpl;
import orgshang.base.util.AjaxResult;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuServiceImpl menuService;
    /**
     * 查询某用户他拥有的菜单树
     * 解决方案:
     *  方案一: 在后端获取当前登陆用户,拿到登陆用户的ID查询菜单树
     *  方案二: 在登陆成功以后,将当前登陆用户传递给前端,前端将登陆用户信息存到浏览器,在进行查询时,前端拿到登录用户的id传给后端(采纳这种)
     * @return
     */
    @GetMapping("/tree")
    public AjaxResult tree(Long userId){
        try {
            System.out.println(userId);
           List<Menu> menus = menuService.tree(userId);
            System.out.println(menus);
            return AjaxResult.me().setData(menus);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询菜单树失败"+e.getMessage());
        }
    }
}
