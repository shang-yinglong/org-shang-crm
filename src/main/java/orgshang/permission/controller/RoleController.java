package orgshang.permission.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import orgshang.anno.RonghuaPermission;
import orgshang.permission.domain.Role;
import orgshang.permission.dto.RolePermissionDto;
import orgshang.permission.query.RoleQuery;
import orgshang.permission.service.impl.RoleServiceImpl;
import orgshang.base.util.AjaxResult;
import orgshang.base.util.PageList;

import java.util.List;
import java.util.Objects;

@RonghuaPermission(name = "角色增删改查")
@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleServiceImpl roleService;
     //新增/修改
    @RonghuaPermission(name = "添加/修改")
    @PutMapping
    public AjaxResult addAndsave(@RequestBody Role role){
        try {
            if (Objects.isNull(role.getId())){
                roleService.add(role);
            }else {
                roleService.update(role);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败"+e.getMessage());
        }


    }
    //根据id删除
    @RonghuaPermission(name="根据id删除")
    @DeleteMapping("/{id}")
    public  AjaxResult deleteById(@PathVariable("id") Long id){
        try {
            roleService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败"+e.getMessage());
        }
    }
    //批量删除
    @RonghuaPermission(name="批量删除")
    @PatchMapping
    public AjaxResult batchDelete(@RequestBody List<Long> ids){
        try {
            roleService.batchDelete(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("批量删除失败"+e.getMessage());
        }
    }
    //根据id查询
    @RonghuaPermission(name="根据id查询")
    @GetMapping("/{id}")
    public AjaxResult getById(@PathVariable("id") Long id){
        try {
           Role role = roleService.getById(id);
            return AjaxResult.me().setData(role);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询失败"+e.getMessage());
        }
    }
    //查询全部
    @RonghuaPermission(name="查询全部")
    @GetMapping
    public AjaxResult getAll(){
        try {
          List<Role>roles =  roleService.getAll();
          return AjaxResult.me().setData(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("查询全部失败"+e.getMessage());
        }
    }
    //分页+高级查询
    @RonghuaPermission(name="分页查询")
    @PostMapping
    public AjaxResult listPage(@RequestBody RoleQuery query){
        try {
           PageList<Role> role =  roleService.listPage(query);
           return AjaxResult.me().setData(role);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("分页查询失败"+e.getMessage());
        }

    }
    @RonghuaPermission(name = "设置权限")
    @PutMapping("/permission")
    public AjaxResult saveRolePermission(@RequestBody RolePermissionDto dto){
        try {
            roleService.saveRolePermission(dto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("设置权限失败!"+e.getMessage());
        }
    }
    @RonghuaPermission(name = "获取角色的权限")
    @GetMapping("/permissionsns/{roleId}")
    public AjaxResult getRolePermissionSns(@PathVariable("roleId")Long roleId){
        try {
            List<String> sns = roleService.getRolePermissionSns(roleId);
            return AjaxResult.me().setData(sns);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("获取角色的权限失败!"+e.getMessage());
        }
    }
}
