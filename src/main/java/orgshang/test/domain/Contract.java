package orgshang.test.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */

@Data
public class Contract extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String sn;
    
                                    private Long orderId;
    
                                    private String orderSn;
    
                                    private Long customerId;
    
                                    private String customerName;
    
                                    private BigDecimal price;
    
        /**
         * 定金
         */
                                    private BigDecimal deposit;
    
                                    private String content;
    
                                    private String description;
    
                                    private Date createTime;
    
                                    private Date updateTime;
    
                                    private Integer state;
    
}
