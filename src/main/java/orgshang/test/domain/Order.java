package orgshang.test.domain;

import java.math.BigDecimal;
import java.util.Date;
import orgshang.base.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */

@Data
public class Order extends BaseDomain{

    private static final long serialVersionUID = 1L;

    
                                    private String sn;
    
                                    private Long businessId;
    
                                    private String businessName;
    
                                    private Long productId;
    
                                    private String productName;
    
        /**
         * 产品的价格+营销活动的力度
         */
                                    private BigDecimal price;
    
                                    private Long customerId;
    
                                    private String customerName;
    
        /**
         * 0-代付款 1- 已付定金 2-已交尾款 3-已完成 -1取消
         */
                                    private Integer state;
    
}
