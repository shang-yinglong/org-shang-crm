package orgshang.test.service.impl;

import orgshang.test.domain.Contract;
import orgshang.test.service.IContractService;
import orgshang.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract> implements IContractService {

}
