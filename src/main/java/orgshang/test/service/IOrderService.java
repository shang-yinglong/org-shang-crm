package orgshang.test.service;

import orgshang.test.domain.Order;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface IOrderService extends IBaseService<Order> {

}
