package orgshang.test.service;

import orgshang.test.domain.Contract;
import orgshang.base.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface IContractService extends IBaseService<Contract> {

}
