package orgshang.test.mapper;

import orgshang.test.domain.Contract;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface ContractMapper extends BaseMapper<Contract> {

}
