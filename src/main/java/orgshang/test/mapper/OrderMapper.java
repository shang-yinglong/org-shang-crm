package orgshang.test.mapper;

import orgshang.test.domain.Order;
import orgshang.base.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author syl
 * @since 2022-12-21
 */
public interface OrderMapper extends BaseMapper<Order> {

}
