package orgshang.login.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import orgshang.login.constant.LoginConstant;
import orgshang.login.dto.LoginDTO;
import orgshang.login.service.ILoginService;
import orgshang.org.domain.Employee;
import orgshang.org.service.IEmployeeService;
import orgshang.base.util.AjaxResult;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Service
public class LoginServiceImpl implements ILoginService {
    @Autowired
    private IEmployeeService employeeService;


    @Override
    public AjaxResult login(LoginDTO dto) {
        String username = dto.getUsername();
        String password = dto.getPassword();
        // 1 参数非空校验
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            // 在这个地方抛出一个运行时的异常,controller层中因为捕获了异常,就会在catch中统一处理,统一返回
            throw new RuntimeException("用户名和密码不能为空，请检查后再提交");
        }
        // 2 根据username去t_employee查询数据
        Employee employee = employeeService.getByUsername(username);
        System.out.println(employee);
        // 2.1 如果employee为空,说明用户名错误,返回错误信息
        if (Objects.isNull(employee)){
            throw new RuntimeException("用户名或密码错误!请检查后重新提交!");

        }
        // 3 判断密码是否正确
        if (!password.equals(employee.getPassword())){
            // 3.1 密码匹配不成功,就返回错误信息
            throw new RuntimeException("用户名或密码错误!请检查后重新提交!");
        }

        // 4 通过uuid生成一个随机字符串作为token
        String token = UUID.randomUUID().toString();
        // 5 将token作为map的key,employee作为map的值,存入map
        LoginConstant.loginMap.put(token, employee);

        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("token",token);
        employee.setPassword("");
        resultMap.put("loginUser",employee);
        // 6 携带token返回前端
        return AjaxResult.me().setData(resultMap);
    }
}
