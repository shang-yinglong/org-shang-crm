package orgshang.login.service;

import orgshang.login.dto.LoginDTO;
import orgshang.base.util.AjaxResult;

public interface ILoginService {
    AjaxResult login(LoginDTO dto);
}
