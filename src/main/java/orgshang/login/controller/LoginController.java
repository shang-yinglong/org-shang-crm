package orgshang.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import orgshang.login.constant.LoginConstant;
import orgshang.login.dto.LoginDTO;
import orgshang.login.service.impl.LoginServiceImpl;
import orgshang.base.util.AjaxResult;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {
    @Autowired
    private LoginServiceImpl loginService;

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginDTO dto) {
        try {
            return loginService.login(dto);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("登录失败"+e.getMessage());
        }
    }

    @PostMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        try {
            //获取前端请求头中的token
            String token = request.getHeader("token");
            //删除map中token对应的登录信息
            LoginConstant.loginMap.remove(token);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("登出失败"+e.getMessage());
        }
    }

}
